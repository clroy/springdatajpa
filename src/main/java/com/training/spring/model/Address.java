package com.training.spring.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Entity
public class Address {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="address_id")
	private Integer id;
	private Integer nbStreet;
	private String street;
	private Integer zipCode;
	private String city;
	private String country;

	//@Autowired
	@ManyToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable
	(joinColumns=@JoinColumn(name="address_id"),
	inverseJoinColumns=@JoinColumn(name="employee_id"))
	private List<Employee> listEmployee;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNbStreet() {
		return nbStreet;
	}
	public void setNbStreet(Integer nbStreet) {
		this.nbStreet = nbStreet;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public Integer getZipCode() {
		return zipCode;
	}
	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public List<Employee> getEmployee() {
		return listEmployee;
	}
	public void setEmployee(List<Employee> employee) {
		this.listEmployee = employee;
	}
	public Address() {
		super();
	}

	public Address(Integer nbStreet, String street, Integer zipCode, String city, String country,
			List<Employee> listEmployee) {
		super();
		this.nbStreet = nbStreet;
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
		this.country = country;
		this.listEmployee = listEmployee;
	}
	@Override
	public String toString() {
		return "Address [getId()=" + getId() + ", getNbStreet()=" + getNbStreet() + ", getStreet()=" + getStreet()
		+ ", getZipCode()=" + getZipCode() + ", getCity()=" + getCity() + ", getCountry()=" + getCountry()
		+ ", getEmployee()=" + getEmployee() + "]";
	}

}
