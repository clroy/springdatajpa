package com.training.spring.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Component
@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="employee_id")
	private Integer id;
	private String login;
	private String password;
	private String nom;
	private String prenom;
	private String email;
	private String role;
	
	
	@ManyToMany (mappedBy="listEmployee")
	private List<Address> addressList;

	
	@OneToMany (mappedBy="employee")
	private List<Notes> notesList;

	public List<Notes> getNotesList() {
		return notesList;
	}
	public void setNotesList(List<Notes> notesList) {
		this.notesList = notesList;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Employee() {
		super();
	}
	public Employee( String login, String password, String nom, String prenom, String email, String role) {
		super();

		this.login = login;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.role = role;
	}

	public String toString() {
		return "Id : " + getId() +
				"\nLogin : " + getLogin() +
				"\nPassword : " + getPassword() +
				"\nNom : " + getNom() +
				"\nPrénom : " + getPrenom() +
				"\nEmail : " + getEmail() + 
				"\nRole : " + getRole();
	}
	public List<Address> getAddressList() {
		return addressList;
	}
	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}


}
