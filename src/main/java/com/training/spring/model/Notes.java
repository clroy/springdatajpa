package com.training.spring.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

@Component
@Entity
public class Notes {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Integer sport;
	private Integer social;
	private Integer performance;
	private Integer assiduite;
	private LocalDate annee;
	private Float moyenne;
	
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn
	private Employee employee;


	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Integer getSport() {
		return sport;
	}
	public void setSport(Integer sport) {
		this.sport = sport;
	}
	public Integer getSocial() {
		return social;
	}
	public void setSocial(Integer social) {
		this.social = social;
	}
	public Integer getPerformance() {
		return performance;
	}
	public void setPerformance(Integer performance) {
		this.performance = performance;
	}
	public Integer getAssiduite() {
		return assiduite;
	}
	public void setAssiduite(Integer assiduite) {
		this.assiduite = assiduite;
	}
	public LocalDate getAnnee() {
		return annee;
	}
	public void setAnnee(LocalDate annee) {
		this.annee = annee;
	}
	public Notes() {
		super();
	}
	public Notes(Integer id, Integer sport, Integer social, Integer performance, Integer assiduite, LocalDate annee) {
		super();
		this.id = id;
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
		this.annee = annee;
	}
	@Override
	public String toString() {
		return "Notes [getSport()=" + getSport() + ", getSocial()=" + getSocial() + ", getPerformance()="
				+ getPerformance() + ", getAssiduite()=" + getAssiduite() + ", getAnnee()=" + getAnnee() + "]";
	}


public Float calculMoy() {
	moyenne= ((this.getSport()+this.getSocial()+this.getPerformance()+this.getAssiduite())/4f);
	return moyenne;
}

}
