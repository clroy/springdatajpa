package com.training.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.training.spring.model.Address;

import com.training.spring.repo.AddressRepository;


@Service
public class AddressService {
	
	@Autowired
	private AddressRepository addressRepository;
	
	public void creataOrUpdateAddress(Address add) {
		this.addressRepository.save(add);
	}

	public void deleteUserAddress(Address add) {
		this.addressRepository.delete(add);
	}
	
	public List<Address> findAll(){
		return this.addressRepository.findAll();
	}
	
	public Page<Address> findAll(Pageable pageable){
		return this.addressRepository.findAll(pageable);
	}
	
	public List<Address> findByCity(String v) {
		return this.addressRepository.findByCity(v);
	}
	
	public Page<Address> findByCity(String v, Pageable pageable){
		return this.addressRepository.findByCity(v, pageable);
	}
	
	public List<Address> findDistinctByCountry(String country){
		return this.findDistinctByCountry(country);
	}
	
	public Address findByNbStreetAndStreet(String n, String s) {
		return this.findByNbStreetAndStreet(n, s);
	}
}
