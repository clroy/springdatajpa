package com.training.spring.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.training.spring.model.Notes;
import com.training.spring.repo.NotesRepository;

@Service
public class NotesService {

	@Autowired
	private NotesRepository notesRepository;

	public void createOrUpdateNotes(Notes nt) {
		this.notesRepository.save(nt);
	}

	public void deleteNotes(Notes nt) {
		this.notesRepository.delete(nt);
	}

	public List<Notes> findByDate(LocalDate d){
		return this.notesRepository.findByAnnee(d);
	}

	public Page<Notes> findByDate(LocalDate d, Pageable pageable){
		return this.notesRepository.findByAnnee(d, pageable);
	}

	public List<Notes> findBySport(Integer sp){
		return this.notesRepository.findBySport(sp);
	}

	public Page<Notes> findBySport(Integer sp, Pageable pageable){
		return this.notesRepository.findBySport(sp, pageable);
	}

	public List<Notes> findBySocial(Integer so){
		return this.notesRepository.findBySocial(so);
	}

	public Page<Notes> findBySocial(Integer so, Pageable pageable){
		return this.notesRepository.findBySocial(so, pageable);
	}

	public List<Notes> findByPerformance(Integer p){
		return this.notesRepository.findByPerformance(p);
	}

	public Page<Notes> findByPerformance(Integer p, Pageable pageable){
		return this.notesRepository.findByPerformance(p, pageable);
	}

	public List<Notes> findByAssiduite(Integer a){
		return this.notesRepository.findByAssiduite(a);
	}

	public Page<Notes> findByAssiduite(Integer a, Pageable pageable){
		return this.notesRepository.findByAssiduite(a, pageable);
	}

	public List<Notes> findAll(){
		return this.notesRepository.findAll();
	}

	public Page<Notes> findAll(Pageable pageable){
		return this.notesRepository.findAll(pageable);
	}
}
