package com.training.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.training.spring.model.Employee;
import com.training.spring.repo.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	public void creataOrUpdateEmployee(Employee emp) {
		this.employeeRepository.save(emp);
	}

	public void deleteUser(Employee emp) {
		this.employeeRepository.delete(emp);
	}

	public List<Employee> findAll() {
		return this.employeeRepository.findAll();
	}

	public Employee findByLoginAndPassword(String login, String password) {
		return this.employeeRepository.findByLoginAndPassword(login, password);
	}

	public Page<Employee> findAll(Pageable pageable) {
		return this.employeeRepository.findAll(pageable);
	}

	public List<Employee> findByNom(String nom){
		return this.employeeRepository.findByNom(nom);
	}

	public Page<Employee> findByNom(String nom, Pageable pageable){
		return this.employeeRepository.findByNom(nom, pageable);
	}

	public List<Employee> findDistinctByRole(String role){
		return this.employeeRepository.findDistinctByRole(role);
	}

}
