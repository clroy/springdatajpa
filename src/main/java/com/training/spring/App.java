package com.training.spring;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.training.spring.config.AppConfig;
import com.training.spring.config.JPAConfig;
import com.training.spring.model.Address;
import com.training.spring.model.Employee;
import com.training.spring.model.Notes;
import com.training.spring.service.AddressService;
import com.training.spring.service.EmployeeService;
import com.training.spring.service.NotesService;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{
		AnnotationConfigApplicationContext context= new AnnotationConfigApplicationContext();
		context.register(AppConfig.class, JPAConfig.class);
		context.refresh();

		/*EmployeeService empService = context.getBean(EmployeeService.class);

		List<Employee> empList = empService.findAll();

		Employee empLP = empService.findByLoginAndPassword("Titi",  "1234");

		for (Employee employee : empList) {
			System.out.println("Login = "+employee.getLogin()+" Password = "+employee.getPassword());
		}
		System.out.println("User nom : "+empLP.getNom());*/


		Employee emp=(Employee)context.getBean(Employee.class);

		emp.setLogin("onart");
		emp.setPassword("1234");
		emp.setNom("Nart");
		emp.setPrenom("ol");
		emp.setEmail("onart@o.com");
		emp.setRole("dev");

		//System.out.println(emp);

		EmployeeService empServ=(EmployeeService)context.getBean(EmployeeService.class);

		//empServ.creataOrUpdateEmployee(emp);

		Address add=(Address)context.getBean(Address.class);

		add.setNbStreet(12);
		add.setStreet("rue blehr");
		add.setZipCode(75000);
		add.setCity("Paris");
		add.setCountry("fr");

		//System.out.println(add);

		AddressService addServ=(AddressService)context.getBean(AddressService.class);

		addServ.creataOrUpdateAddress(add);

		ArrayList<Address> addList= new ArrayList<Address>();

		addList.add(add);

		ArrayList<Employee> empList= new ArrayList<Employee>();

		empList.add(emp);

		//System.out.println(addList);

		add.setEmployee(empList);

		emp.setAddressList(addList);



		//addServ.creataOrUpdateAddress(add);


		//System.out.println(add.getEmployee());
		//System.out.println(emp.getAddressList());

		Notes nt=(Notes)context.getBean(Notes.class);
		nt.setSport(5);
		nt.setSocial(1);
		nt.setPerformance(1);
		nt.setAssiduite(0);
		nt.setAnnee(LocalDate.ofYearDay(2017, 1));
		nt.calculMoy();

		ArrayList<Notes> ntList=new ArrayList<Notes>();

		ntList.add(nt);

		emp.setNotesList(ntList);
		nt.setEmployee(emp);


		//System.out.println(nt);
		//System.out.println(nt.calculMoy());

		NotesService ntServ=(NotesService)context.getBean(NotesService.class);

		ntServ.createOrUpdateNotes(nt);



		/*Employee emp2=(Employee)context.getBean(Employee.class);

		emp2.setLogin("kwenn");
		emp2.setPassword("5678");
		emp2.setNom("wenn");
		emp2.setPrenom("ka");
		emp2.setEmail("kwenn@o.com");
		emp2.setRole("wm");

		//empList.add(emp2);

		//add.setEmployee(empList);

		//emp2.setAddressList(addList);

		empServ.creataOrUpdateEmployee(emp2);*/
		empServ.creataOrUpdateEmployee(emp);

		Pageable firstPageWithTwoElements = PageRequest.of(0, 20);


		Page<Employee> allEmp=empServ.findAll(firstPageWithTwoElements);
		for (Employee employee : allEmp) {
			System.out.println(employee);
		}
	}


}
