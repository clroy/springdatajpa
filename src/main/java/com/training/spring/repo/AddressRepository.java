package com.training.spring.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.spring.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{
	public List<Address> findByCity(String v);

	public Page<Address> findByCity(String v, Pageable pageable);

	public List<Address> findDistinctByCountry(String country);

	public Address findByNbStreetAndStreet(String n, String s);

	public Page<Address> findAll(Pageable pageable);

}
