package com.training.spring.repo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.training.spring.model.Notes;

@Repository
public interface NotesRepository extends JpaRepository<Notes, Integer>{

	public List<Notes> findByAnnee(LocalDate d);

	public Page<Notes> findByAnnee(LocalDate d, Pageable pageable);

	public List<Notes> findBySport(Integer sp);

	public Page<Notes> findBySport(Integer sp, Pageable pageable);

	public List<Notes> findBySocial(Integer so);

	public Page<Notes> findBySocial(Integer so, Pageable pageable);

	public List<Notes> findByPerformance(Integer p);

	public Page<Notes> findByPerformance(Integer p, Pageable pageable);

	public List<Notes> findByAssiduite(Integer a);

	public Page<Notes> findByAssiduite(Integer a, Pageable pageable);

	public List<Notes> findAll();

	public Page<Notes> findAll(Pageable pageable);
}
